# meta-emacs - Emacs recipes for OpenEmbedded #

meta-emacs contains OpenEmbedded recipes for GNU Emacs.

At the moment it is only Emacs version 26.1, but the goal is to add
recipes first for prereleases and master and later for earlier
versions.

meta-emacs also contains recipes for several Emacs dependencies that
were not found elsewhere, such as mailutils, openmotif, and libotf. 

The PACKAGECONFIG for the emacs recipe supports almost all of the
(relevant) configuration options, which means you can build with any
of the supported graphical toolkits and support libraries. 


## The recipes

### Emacs

#### Emacs 26.1

Emacs depends unconditionally on [`ncurses`](http://layers.openembedded.org/layerindex/branch/master/recipes/?q=ncurses "ncurses on layerindex").

The following `PACKAGECONFIG`s are defined:

  * Features without dependencies
      * profiling `--(enable|disable)-profiling`
      * check-lisp-object-type `--(enable|disable)-check-lisp-object-type`
      * link-time-optimization `--(enable|disable)-link-time-optimization`
      * wide-int `--(with|without)-wide-int`
      * modules  `--(with|without)-modules`
      * threads  `--(with|without)-threads`
      * makeinfo  `--(with|without)-makeinfo` (May depend on  [`texinfo-native`](http://layers.openembedded.org/layerindex/branch/master/recipes/?q=texinfo))
      * compress-install  `--(with|without)-compress-install`
  * Security features
      * acl --(enable|disable)-acl acl
      * selinux  --(with|without)-selinux libselinux
  * Mail features
      * mailutils --(with|without)-mailutils mailutils
      * pop  --(with|without)-pop
      * kerberos5 --(with|without)-kerberos5 krb5
      * hesiod --(with|without)-hesiod  hesiod
      * mail-unlink --(with|without)-mail-unlink
  * Sound  --witout-sound if neither alsa not oss is set
      * alsa --with-sound=alsa alsa-lib
      * oss --with-sound=oss
  * GUI Toolkits
    Any of the X toolkits add dependencies on libxrandr, libxinerama,
    libxfixes, and libxext.
    --with-x=no is passed to configure if none of the toolkits are specified, 
      * gtk2   --with-x=yes --with-x-toolkit=gtk2   gtk+ 
      * gtk3   --with-x=yes --with-x-toolkit=gtk3   gtk+3
        * xwidgets --(with|without)-xwidgets webkitgtk
      * lucid  --with-x=yes --with-x-toolkit=lucid
      * motif  --with-x=yes --with-x-toolkit=motif openmotif
      * cairo  --with-x=yes --with-cairo cairo
  * Toolkit scrollbars
      * toolkit-scroll-bars --(with|without)-toolkit-scroll-bars Only
        works with the lucid toolkit.
      * xaw3d --(with|without)-xaw3d libxaw3d Only works with the
        lucid toolkit
  * Image support
      * xpm  --(with|without)-xpm libxpm
      * jpeg  --(with|without)-jpeg jpeg
      * tiff  --(with|without)-tiff tiff
      * gif  --(with|without)-gif giflib
      * png  --(with|without)-png libpng
      * rsvg  --(with|without)-rsvg  librsvg
      * imagemagick --(with|without)-imagemagick  imagemagick6
  * Other
      * lcms2  --(with|without)-lcms2  lcms
      * xml2  --(with|without)-xml2 libxml2"
      * xim  --(with|without)-xim libgxim"
      * gpm  --(with|without)-gpm gpm"
      * gnutls  --(with|without)-gnutls gnutls"
      * zlib  --(with|without)-zlib zlib
  * System
      * libsystemd --(with|without)-libsystemd systemd
      * gconf  --(with|without)-gconf gconf
      * gsettings  --(with|without)-gsettings glib-2.0
      * dbus  --(with|without)-dbus dbus
  * Font
      * xft  --(with|without)-xft libxft
      * libotf  --(with|without)-libotf libotf
      * m17n-flt  --(with|without)-m17n-flt m17n-flt
  * File notification
    --with-file-notification=no if none of inotofy or gfile if given.
      * inotify  --with-file-notification=inotify
      * gfile  --with-file-notification=gfile glib-2.0


