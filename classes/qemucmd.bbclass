# Provide a QEMUCMD variable that can be passed to Makefiles that has
# to run some tool through user-qemu.

DEPENDS += "qemu-native"

inherit qemu

def qemucmd(d, appended_library_paths=None):
    rootfs_path = d.getVar('RECIPE_SYSROOT')
    return qemu_wrapper_cmdline(
        d, rootfs_path,
        [rootfs_path + d.getVar(v, False)
         for v in ('libdir', 'base_libdir')] +
        (list(appended_library_paths) if appended_library_paths else []))

QEMUCMD = "env ${@qemucmd(d)}"
QEMUCMD_class-native = ""
export QEMUCMD
