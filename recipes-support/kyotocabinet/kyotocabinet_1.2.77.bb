HOMEPAGE ="https://fallabs.com/kyotocabinet/"
SUMMARY = "Kyoto Cabinet is a library of routines for managing a database."
DESCRIPTION = "Kyoto Cabinet is a library of routines for managing a \
database.  The database is a simple data file containing records, each is a \
pair of a key and a value.  Every key and value is serial bytes with variable \
length.  Both binary data and character string can be used as a key and a \
value.  Each key must be unique within a database. There is neither concept of \
data tables nor data types.  Records are organized in hash table or B+ tree."

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = "https://fallabs.com/kyotocabinet/pkg/kyotocabinet-${PV}.tar.gz \
           file://0001-Remove-hard-coded-include-and-lib-paths.patch \
           "
SRC_URI[md5sum] = "0f1fa6d10cb5501ebc0ad6ded7a90f68"
SRC_URI[sha256sum] = "56899329384cc6f0f1f8aa3f1b41001071ca99c1d79225086a7f3575c0209de6"

DEPENDS = "zlib xz lzo"

inherit autotools-brokensep

PACKAECONFIG = "lzma lzo zlib"
PACKAGECONFIG[lzma] = "--enable-lzma,--disable-lzma,lzma"
PACKAGECONFIG[lzo] = "--enable-lzo,--disable-lzo,lzo"
PACKAGECONFIG[zlib] = "--enable-zlib,--disable-zlib,zlib"
