HOMEPAGE = "https://github.com/achernya/hesiod"
SUMMARY = "Hesiod name service library"
DESCRIPTION = "Hesiod name service library. \
Hesiod can provide general name service for a variety of applications \
and is based on the Berkeley Internet Name Daemon (BIND)."

LICENSE = "BSD-2-Clause & ISC"
LIC_FILES_CHKSUM = "file://COPYING;md5=320d2dc9208b6d01fe68e7e4dedbf51d"

DEPENDS = "libidn"

SRC_URI = "git://git@github.com/achernya/hesiod.git;protocol=ssh"

SRCREV = "30fa0206c7b1cdae5cd7200cdf1feb03838cf3af"

S = "${WORKDIR}/git"

inherit autotools

# Specify any options you want to pass to the configure script using EXTRA_OECONF:
EXTRA_OECONF = ""
