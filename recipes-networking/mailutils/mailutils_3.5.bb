SUMMARY = "Mailutils is a swiss army knife of electronic mail handling."
DESCRIPTION = "Mailutils is a swiss army knife of electronic mail handling. \
It offers a rich set of utilities and daemons for processing e-mail. \
\
All Mailutils programs are able to operate on mailboxes of any existing format, \
ranging from standard UNIX maildrops, through maildir and up to remote \
mailboxes, which are transparently accessed using IMAP4, POP3 and SMTP. "
HOMEPAGE = "https://mailutils.org/"

LICENSE = "GPLv3 & LGPLv3 & GFDL-1.2"
LIC_FILES_CHKSUM = "file://COPYING.LESSER;md5=e98418c9f7fb46153ded80b5e4b0aea7 \
                    file://COPYING;md5=754ea89500f3dffdb1c31cd0cc36f101 \
                    file://doc/texinfo/COPYING.DOC;md5=2fc49995c913cc5b6d41eb82e432b2e8"

SRC_URI = "https://ftp.gnu.org/gnu/mailutils/mailutils-${PV}.tar.xz \
           file://0001-Fix-the-check-for-libresolv.patch \
           file://0002-gsasl.m4-Disable-the-gsasl-version-check.patch \
           file://0003-tls.m4-Disable-the-gnutls-version-check.patch \
           file://0004-configure.ac-Only-compile-to-determine-FRIBIDI_CHARS.patch \
           file://0005-configure.ac-Use-QEMU-to-check-berkeley-DB-lib.patch \
           file://0006-conficure.ac-Print-nntp-status.patch \
           "
SRC_URI[md5sum] = "c3d93a143a7920edd5fcc20a658ca9cf"
SRC_URI[sha256sum] = "bac687b5fdb139fcc569dd2f32f21663cc95e378e721b69488963b347839a2f3"

# NOTE: the following prog dependencies are unknown, ignoring: python-config
# NOTE: the following library dependencies are unknown, ignoring: ltdl ldap
#       (this is based on recipes that have previously been built and packaged)

DEPENDS = "bison-native flex-native libtool libxcrypt"

# TODO: Default mailbox scheme can only be set via MU_DEFAULT_SCHEME

PACKAGECONFIG = " \
  ${@bb.utils.filter('DISTRO_FEATURES','pam ipv6', d)} \
  tcp-wrappers \
  readline \
  gssapi gsasl gnutls fribidi \
  virtual-domains imap pop mh maildir smtp sendmail prog \
  build-pop3d build-imap4d build-comsat \
  build-frm build-maidag build-mail build-sieve build-messages \
  build-readmsg build-dotlock build-movemail build-mimeview \
  build-mh  \
  gdbm berkeley-db tokyocabinet kyotocabinet \
  ldap \
  mysql postgres"

NODBM = "${@bb.utils.contains_any('PACKAGECONFIG', \
     'gdbm berkeley-db tokyocabinet kyotocabinet', '', '--without-dbm', d)}"
EXTRA_OECONF += "${NODBM} --enable-build-clients --enable-build-servers"

PACKAGECONFIG[experimental] = "--enable-experimental,--disable-experimental"
PACKAGECONFIG[pam] = "--enable-pam,--disable-pam,libpam"
PACKAGECONFIG[ipv6] = "--enable-ipv6,--disable-ipv6"
PACKAGECONFIG[virtual-domains] = "--enable-virtual-domains,--disable-virtual-domains"
PACKAGECONFIG[imap] = "--enable-imap,--disable-imap"
PACKAGECONFIG[pop] = "--enable-pop,--disable-pop"
# Only works with experimental?
PACKAGECONFIG[nntp] = "--enable-nntp,--disable-nntp"
PACKAGECONFIG[mh] = "--enable-mh,--disable-mh"
PACKAGECONFIG[maildir] = "--enable-maildir,--disable-maildir"
PACKAGECONFIG[smtp] = "--enable-smtp,--disable-smtp"
PACKAGECONFIG[sendmail] = "--enable-sendmail,--disable-sendmail"
PACKAGECONFIG[prog] = "--enable-prog,--disable-prog"
# No recipe for GNU Radius yet, maybe never
#PACKAGECONFIG[radius] = "--enable-radius,--disable-radius,radius"
# Needs more work to work
#PACKAGECONFIG[python] = "--enable-python,--disable-python"
# TODO: Also requires experimental
PACKAGECONFIG[cxx] = "--enable-cxx,--disable-cxx"
PACKAGECONFIG[tcp-wrappers] = "--with-tcp-wrappers,--without-tcp-wrappers,tcp-wrappers"
PACKAGECONFIG[readline] = "--with-readline,--without-readline,readline"
# mailutils can probably use other gssapi implementaions like gnugss and
# heimdal, but krb5 seems like the dominating one.
PACKAGECONFIG[gssapi] = "--with-gssapi,--without-gssapi,krb5"
PACKAGECONFIG[gsasl] = "--with-gsasl,--without-gsasl,libgsasl"
PACKAGECONFIG[gnutls] = "--with-gnutls,--without-gnutls,gnutls"
PACKAGECONFIG[gdbm] = "--with-gdbm,--without-gdbm,gdbm"
PACKAGECONFIG[berkeley-db] = "--with-berkeley-db,--without-berkeley-db,db"
PACKAGECONFIG[tokyocabinet] = "--with-tokyocabinet,--without-tokyocabinet,tokyocabinet"
PACKAGECONFIG[kyotocabinet] = "--with-kyotocabinet,--without-kyotocabinet,kyotocabinet"
PACKAGECONFIG[fribidi] = "--with-fribidi,--without-fribidi,fribidi"
PACKAGECONFIG[mysql] = "--with-mysql,--without-mysql,mariadb"
PACKAGECONFIG[postgres] = "--with-postgres,--without-postgres,postgresql"
PACKAGECONFIG[ldap] = "--with-ldap,--without-ldap,openldap"
# No recipe available for guile 2.4
#PACKAGECONFIG[guile] = "--with-guile,--without-guile,guile"

inherit gettext pythonnative perlnative autotools qemucmd

export QEMU = "${QEMUCMD}"

PACKAGE_BEFORE_PN = "libmailutils"
FILES_libmailutils = "${libdir}"
FILES_${PN}-dev += "${bindir}/mailutils-config"
