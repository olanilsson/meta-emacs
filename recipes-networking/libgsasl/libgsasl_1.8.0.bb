HOMEPAGE = "https://www.gnu.org/software/gsasl/"
SUMMARY = "Simple Authentication and Security Layer framework and a few common SASL mechanisms."

LICENSE = "GPLv3 & LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504 \
                    file://COPYING.LIB;md5=4fbd65380cdd255951079008b364516c"

SRC_URI = "ftp://ftp.gnu.org/gnu/gsasl/libgsasl-${PV}.tar.gz \
           file://0001-Add-AM_PROG_AR-to-configure.ac.patch \
           "
SRC_URI[md5sum] = "5dbdf859f6e60e05813370e2b193b92b"
SRC_URI[sha256sum] = "3adfb49f9c92a719dea855fd1840d698cde55d4648d332a69032ba8bea207720"

# NOTE: the following prog dependencies are unknown, ignoring: krb5-config

DEPENDS = "libidn"

inherit gettext autotools

PACKAGECONFIG = " \
    ntlm \
    mitgss \
"

PACKAGECONFIG[ntlm] = "--enable-ntlm,--disable-ntlm,libntlm"
PACKAGECONFIG[gnugss] = "--with-gssapi-impl=gss,,gss"
PACKAGECONFIG[mitgss] = "--with-gssapi-impl=mit,,krb5"
# shishi is not usable at this point
PACKAGECONFIG[krb5] = "--enable-kerberos_v5,--disable-kerberos_v5,shishi"

do_configure[prefuncs] += "libgsasl_verify_pkgconfig"

python libgsasl_verify_pkgconfig(){
    packageconfig = frozenset((d.getVar('PACKAGECONFIG') or '').split())
    # check gss options, max one
    gssvariants = {'gnugss', 'mitgss', 'heimdalgss'}
    if len(packageconfig & gssvariants) > 1:
        bb.fatal('Multiple gss implementaions (',
                 ', '.join(packageconfig & gssvariants),
                 ') selected, only one allowed')
}

NOGSS = " ${@bb.utils.contains_any('PACKAGECONFIG', 'gnugss mitgss', '', '--with-gssapi-impl=no', d)}"
EXTRA_OECONF_append = " ${NOGSS}"
