SUMMARY = "Libntlm implement the Microsoft NTLM authentication protocol."
HOMEPAGE = "http://www.nongnu.org/libntlm/"

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

SRC_URI = "http://www.nongnu.org/libntlm/releases/libntlm-${PV}.tar.gz"
SRC_URI[md5sum] = "375a3a87d29b61da5466f9b06325ad62"
SRC_URI[sha256sum] = "53d799f696a93b01fe877ccdef2326ed990c0b9f66e380bceaf7bd9cdcd99bbd"

inherit autotools
