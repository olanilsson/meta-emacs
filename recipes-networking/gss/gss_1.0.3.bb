SUMMARY = "Generic Security Service (GSS), a free implementation of RFC 2743/2744."
HOMEPAGE = "https://www.gnu.org/software/gss/"

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=f27defe1e96c2e1ecd4e0c9be8967949"

SRC_URI = "http://ftp.gnu.org/gnu/gss/gss-${PV}.tar.gz"
SRC_URI[md5sum] = "441859b30bed73010d426c087c71aac5"
SRC_URI[sha256sum] = "ff919ddc731531d65e27d7ababdc361aae05ada5f1a6dd60703d153307dcdeeb"

inherit gettext autotools

PACKAGECONFIG = ""

# Cannot get shishi to build at the moment
PACKAGECONFIG[krb5] = "--enable-kerberos5,--disable-kerberos5,shishi"

EXTRA_OECONF = "--disable-gtk-doc --disable-gtk-doc-html --disable-gtk-doc-pdf"
