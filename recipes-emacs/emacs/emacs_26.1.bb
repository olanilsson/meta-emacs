SUMMARY = "Emacs is the extensible, customizable, self-documenting real-time display editor"
HOMEPAGE = "https://www.gnu.org/software/emacs/"

LICENSE = "GPL-3.0 & GFDL-1.3"
LIC_FILES_CHKSUM = " \
  file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://leim/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://lwlib/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://src/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://etc/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://lib/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://lib-src/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://doc/lispintro/doclicense.texi;md5=ec5d9a3fb24cf9a19809c07143b94b0f \
  file://doc/emacs/doclicense.texi;md5=ec5d9a3fb24cf9a19809c07143b94b0f \
  file://doc/lispref/doclicense.texi;md5=ec5d9a3fb24cf9a19809c07143b94b0f \
  file://doc/misc/doclicense.texi;md5=ec5d9a3fb24cf9a19809c07143b94b0f \
  file://lisp/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://lisp/emacs-lisp/copyright.el;md5=445b47c04c4f4bdaf4dfb3ab04c21b4e \
  file://lisp/emacs-lisp/copyright.elc;md5=f56a707bbf166e47bedf4359bb908c3d \
  file://admin/unidata/copyright.html;md5=f620dc5d1504aa7d8037d89eeea0352f \
  file://admin/notes/copyright;md5=cac5140faf0bca3a07af9c5ae3c92d16 \
  file://msdos/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
  file://oldXMenu/copyright.h;md5=34e7f1dee98a49397efe264c165ebf75 \
  file://nt/COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
"

SRC_URI = "https://ftp.gnu.org/gnu/emacs/emacs-${PV}.tar.xz \
           file://0001-src-Run-make-docfile-in-QEMU.patch \
           file://0002-src-Run-temacs-in-QEMU-without-address-randomization.patch \
           file://0003-lisp-Run-emacs-in-QEMU.patch \
           file://0004-lib-src-Run-emacs-in-QEMU-for-blessmail.patch \
           file://0005-disable-clever-owner-hacks-in-install-arch-indep.patch \
           "
SRC_URI[md5sum] = "649ec46965a8b842bdb2cbf7764c2a9a"
SRC_URI[sha256sum] = "1cf4fc240cd77c25309d15e18593789c8dbfba5c2b44d8f77c886542300fd32c"

LIBLOCKFILE = "${@bb.utils.contains('PACKAGECONFIG', 'mailutils', '', 'liblockfile', d)}"
DEPENDS = "ncurses qemu-native ${LIBLOCKFILE}"

inherit autotools pkgconfig qemucmd

# emacs does not use Automake, so check for Makefile.in-files instead
CONFIGURE_FILES += "Makefile.in"

EXTRA_OECONF = "--disable-build-details --disable-silent-rules"

DEFAULTPACKAGECONFIG = " \
  ${@bb.utils.filter('DISTRO_FEATURES', 'selinux acl', d)} \
  gtk3 toolkit-scroll-bars xim gpm \
  xpm jpeg tiff gif png rsvg lcms2 imagemagick \
  xml2 alsa \
  xft libotf m17n-flt \
  dbus gsettings zlib \
  threads makeinfo mailutils compress-install  \
"
PACKAGECONFIG = "${DEFAULTPACKAGECONFIG}"

def enableopt(option):
    return '--enable-{option},--disable-{option}'

XDEPENDS = "libxrandr libxinerama libxfixes libxext"

#PACKAGECONFIG[profiling] = "${@enableopt('profiling')}"
PACKAGECONFIG[profiling] = "--enable-profiling,--disable-profiling"
PACKAGECONFIG[check-lisp-object-type] = "--enable-check-lisp-object-type,--disable-check-lisp-object-type"
PACKAGECONFIG[link-time-optimization] = "--enable-link-time-optimization,--disable-link-time-optimization"
PACKAGECONFIG[acl] = "--enable-acl,--disable-acl,acl"
PACKAGECONFIG[mailutils] = "--with-mailutils,--without-mailutils,mailutils,mailutils"
PACKAGECONFIG[pop] = "--with-pop,--without-pop"
PACKAGECONFIG[kerberos5] = "--with-kerberos5,--without-kerberos5,krb5"
PACKAGECONFIG[hesiod] = "--with-hesiod,--without-hesiod,hesiod"
PACKAGECONFIG[mail-unlink] = "--with-mail-unlink,--without-mail-unlink"
PACKAGECONFIG[alsa] = "--with-sound=alsa,,alsa-lib"
PACKAGECONFIG[oss] = "--with-sound=oss"
PACKAGECONFIG[gtk2] = "--with-x-toolkit=gtk2,,gtk+ ${XDEPENDS}"
PACKAGECONFIG[gtk3] = "--with-x-toolkit=gtk3,,gtk+3 ${XDEPENDS}"
PACKAGECONFIG[lucid] = "--with-x-toolkit=lucid,,${XDEPENDS}"
PACKAGECONFIG[motif] = "--with-x-toolkit=motif,,openmotif ${XDEPENDS}"
PACKAGECONFIG[wide-int] = "--with-wide-int,--without-wide-int"
PACKAGECONFIG[xpm] = "--with-xpm,--without-xpm,libxpm"
PACKAGECONFIG[jpeg] = "--with-jpeg,--without-jpeg,jpeg"
PACKAGECONFIG[tiff] = "--with-tiff,--without-tiff,tiff"
PACKAGECONFIG[gif] = "--with-gif,--without-gif,giflib"
PACKAGECONFIG[png] = "--with-png,--without-png,libpng zlib"
PACKAGECONFIG[rsvg] = "--with-rsvg,--without-rsvg,librsvg"
PACKAGECONFIG[lcms2] = "--with-lcms2,--without-lcms2,lcms"
PACKAGECONFIG[libsystemd] = "--with-libsystemd,--without-libsystemd,systemd"
PACKAGECONFIG[cairo] = "--with-cairo,--without-cairo,cairo"
PACKAGECONFIG[xml2] = "--with-xml2,--without-xml2,libxml2"
PACKAGECONFIG[imagemagick] = "--with-imagemagick,--without-imagemagick,imagemagick6"
PACKAGECONFIG[xft] = "--with-xft,--without-xft,libxft"
PACKAGECONFIG[libotf] = "--with-libotf,--without-libotf,libotf"
PACKAGECONFIG[m17n-flt] = "--with-m17n-flt,--without-m17n-flt,m17n-flt"
PACKAGECONFIG[toolkit-scroll-bars] = "--with-toolkit-scroll-bars,--without-toolkit-scroll-bars"
PACKAGECONFIG[xaw3d] = "--with-xaw3d,--without-xaw3d,libxaw3d"
PACKAGECONFIG[xim] = "--with-xim,--without-xim,libgxim"
PACKAGECONFIG[gpm] = "--with-gpm,--without-gpm,gpm"
PACKAGECONFIG[dbus] = "--with-dbus,--without-dbus,dbus"
PACKAGECONFIG[gconf] = "--with-gconf,--without-gconf,gconf"
PACKAGECONFIG[gsettings] = "--with-gsettings,--without-gsettings,glib-2.0"
PACKAGECONFIG[selinux] = "--with-selinux,--without-selinux,libselinux"
PACKAGECONFIG[gnutls] = "--with-gnutls,--without-gnutls,gnutls"
PACKAGECONFIG[zlib] = "--with-zlib,--without-zlib,zlib"
PACKAGECONFIG[modules] = "--with-modules,--without-modules"
PACKAGECONFIG[threads] = "--with-threads,--without-threads"
PACKAGECONFIG[inotify] = "--with-file-notification=inotify"
PACKAGECONFIG[gfile] = "--with-file-notification=gfile,,glib-2.0"
PACKAGECONFIG[xwidgets] = "--with-xwidgets,--without-xwidgets,webkitgtk"
MAYBE_MAKEINFO = "${@bb.utils.contains('HOSTTOOLS', 'makeinfo', '', 'texinfo-native', d)}"
PACKAGECONFIG[makeinfo] = "--with-makeinfo,--without-makeinfo,${MAYBE_MAKEINFO}"
PACKAGECONFIG[compress-install] = "--with-compress-install,--without-compress-install"
#   --with-gameuser=USER_OR_GROUP  user for shared game score files. An argument
#                                  prefixed by ':' specifies a group instead.

def without_config(d, configs, without, withany=''):
    return bb.utils.contains_any('PACKAGECONFIG', configs, withany, without, d)

NOFILENOTIFY = "${@without_config(d, 'inotify gfile', '--with-file-notification=no')}"
NOSOUND = "${@without_config(d, 'alsa oss', '--with-sound=no')}"
WITHX = "--with-x=${@without_config(d, 'gtk2 gtk3 motif lucid cairo', 'no', 'yes')}"
EXTRA_OECONF += "${NOFILENOTIFY} ${NOSOUND} ${WITHX}"

do_configure[prefuncs] += "emacs_config_sanity"
python emacs_config_sanity() {
    sane = True
    pkgcfg = set((d.getVar('PACKAGECONFIG') or '').split())
    if 'mailutils' in pkgcfg:
        mailcfgs =  {'pop', 'kerberos5', 'hesiod', 'mail-unlink'} & pkgcfg
        if mailcfgs:
            bb.warn('PACKAGECONFIG "mailutils" makes "', ' '.join(mailcfgs),
                    '" redundant')
    else:
        if 'kerberos5' in pkgcfg and 'pop' not in pkgcfg:
            bb.warn('PACKAGECONFIG "kerberos5" without "pop" is redundant')

    # audio
    if 'alsa' in pkgcfg and 'oss' in pkgcfg:
        bb.warn('PACKAGECONFIG "alsa" makes "oss" redundant')
    # toolkits
    toolkits = {'gtk2', 'gtk3', 'lucid', 'motif'}
    enabled_toolkits = toolkits & pkgcfg
    if len(enabled_toolkits) > 1:
        bb.error('Multiple toolkits in PACKAGECONFIG: ', ' '.join(enabled_toolkits))
        sane = False
    # toolkit-scroll-bars vs xaw3d
    if 'toolkit-scroll-bars' not in pkgcfg:
        if 'motif' in pkgcfg or 'gtk2' in pkgcfg or 'gtk3' in pkgcfg:
            bb.warn('PACKAGECONFIG "motif", "gtk2", and "gtk3" always uses "toolkit-scroll-bars"')
    if 'xaw3d' in pkgcfg and 'lucid' not in pkgcfg:
        bb.warn('PACKAGECONFIG "xaw3d" should only be combined with "lucid"')
    # xwidgets
    if 'xwidgets' in pkgcfg and 'gtk3' not in pkgcfg:
        bb.error('PACKAGECONFIG "xwidgets" requires "gtk3"')
        sane = False
    # fatal errors?
    if not sane:
        bb.fatal('Configuration errors, failing task')
}

do_configure[postfuncs] += "verify_emacs_config"
python verify_emacs_config() {
    cfgmap = {
        'acl': 'USE_ACL',
        'alsa': 'HAVE_ALSA',
        'cairo': 'HAVE_CAIRO',
        'check-lisp-object-type': 'CHECK_LISP_OBJECT_TYPE',
        'dbus': 'HAVE_DBUS',
        'freetype': 'HAVE_FREETYPE',
        'gconf': 'HAVE_GCONF',
        'gfile': 'HAVE_GFILENOTIFY',
        'gif': 'HAVE_GIF',
        'gnutls': 'HAVE_GNUTLS',
        'gpm': 'HAVE_GPM',
        'gsettings': 'HAVE_GSETTINGS',
        'gtk2': 'USE_GTK',
        'gtk3': 'HAVE_GTK3',
        'hesiod': 'HESIOD',
        'imagemagick': 'HAVE_IMAGEMAGICK',
        'inotify': 'HAVE_INOTIFY',
        'jpeg': 'HAVE_JPEG',
        'kerberos5': 'KERBEROS5',
        'kqueue': 'HAVE_KQUEUE',
        'largefile': '_FILE_OFFSET_BITS',
        'lcms2': 'HAVE_LCMS2',
        'libotf': 'HAVE_LIBOTF',
        'libsystemd': 'HAVE_LIBSYSTEMD',
        'lucid': 'USE_LUCID',
        'm17n-flt': 'HAVE_M17N_FLT',
        'mail-unlink': 'MAIL_UNLINK_SPOOL',
        'modules': 'HAVE_MODULES',
        'motif': 'USE_MOTIF',
        'png': 'HAVE_PNG',
        'pop': 'MAIL_USE_POP',
        'rsvg': 'HAVE_RSVG',
        'selinux': 'HAVE_SELINUX',
        'sound': 'HAVE_SOUND', # needs more work
        'threads': 'THREADS_ENABLED',
        'tiff': 'HAVE_TIFF',
        'toolkit-scroll-bars': 'USE_TOOLKIT_SCROLL_BARS',
        'wide-int': 'WIDE_EMACS_INT',
        'xaw3d': 'HAVE_XAW3D',
        'xft': 'HAVE_XFT',
        'xim': 'USE_XIM', # 'HAVE_XIM' is set if libX11 has XIM
                          # support. should probably test for both
        'xml2': 'HAVE_LIBXML2',
        'xpm': 'HAVE_XPM',
        'xwidgets': 'USE_GTK', #TODO and 'HAVE_XWIDGETS',
        'zlib': 'HAVE_ZLIB',
        }
    unchecked = {
        'doc', 'compress-install', 'profiling', 'mailutils',
        'link-time-optimization', 'makeinfo', 'oss',}
    config_h = os.path.join(d.getVar('B'), 'src/config.h')
    packageconfig = (d.getVar('PACKAGECONFIG') or '').split()
    allpkgconfigs = d.getVarFlags('PACKAGECONFIG')
    import re
    verified = True
    with open(config_h) as cfgfile:
        cfgcontent = cfgfile.read()
        for cfg in allpkgconfigs:
            try:
                define = cfgmap[cfg]
                if define is None:
                    continue
            except KeyError:
                if cfg not in unchecked:
                    verified = False
                    bb.error('Unchecked PACKAGECONFIG ', cfg)
                continue
            defmatch = re.search(r'^#define ' + define  + ' (1|64)',
                                 cfgcontent, re.MULTILINE)
            if cfg in packageconfig:
                if not defmatch:
                    verified = False
                    bb.error(cfg, ' in PACKAGECONFIG but ', define, ' not enabled in config.h')
            else:
                if defmatch:
                    if cfg == 'gtk2':
                        continue  # hack for webkit
                    verified = False
                    bb.error(cfg, ' not in PACKAGECONFIG but ', define, ' enabled in config.h')
    if not verified:
        bb.fatal('Fatal QA errors found, failing task.')
}

QEMUARCH = "${TARGET_ARCH}"
QEMUARCH_x86 = "i386"
# Large stack is required at least on x86_64 host, otherwise random segfaults appear:
QEMU = "qemu-${QEMUARCH} ${QEMU_OPTIONS} -L ${RECIPE_SYSROOT} "
EXTRA_OEMAKE += 'QEMU="${QEMU}"'

lispdir = "${datadir}/emacs/${PV}/lisp"

PACKAGES =+ "${PN}-el"

FILES_${PN} += "${datadir} ${systemd_user_unitdir}"
# Actually only .el or .el.gz should be included based on the
# --with-compress-install option, but this is easier. 
FILES_${PN}-el = "\
  ${lispdir}/*.el.gz \
  ${lispdir}/*/*.el.gz \
  ${lispdir}/*/*/*.el.gz \
  ${lispdir}/*/*/*/*.el.gz \
  ${lispdir}/*.el \
  ${lispdir}/*/*.el \
  ${lispdir}/*/*/*.el \
  ${lispdir}/*/*/*/*.el \
"

# The infodir test matches the dired.info file
INSANE_SKIP_emacs-doc = "infodir"

ERROR_QA_append = " emacs-elfiles"
QAPATHTEST[emacs-elfiles] = "emacs_package_qa_elisp"
def emacs_package_qa_elisp(file, name, d, elf, messages):
    elpkg = d.getVar('PN') + '-el'
    if name == elpkg:
        return
    if (file.endswith('.el.gz')
        or (file.endswith('.el')
            and os.path.exists(file + 'c'))):
        package_qa_add_message(messages, "emacs-elfiles",
                               file + ' should be packaged in ' + elpkg)

ERROR_QA_append = " emacs-not-elfiles"
QAPATHTEST[emacs-not-elfiles] = "emacs_package_qa_notelisp"
def emacs_package_qa_notelisp(file, name, d, elf, messages):
    if name != d.getVar('PN') + '-el':
        return
    import re
    expected_ending = '.el.gz'
    if re.search(r'--with(out)?-compress-install(?(1)(=yes)?|=no)',
                 d.getVar('EXTRA_OECONF')):
        expected_ending = '.el'
    if not file.endswith(expected_ending):
        package_qa_add_message(messages, "emacs-not-elfiles",
                               file + ' should not be in ' + name + ', expecting ' + expected_ending)
