SUMMARY = "A Library for handling OpenType Font (OTF)"
DESCRIPTION = "The library 'libotf' provides the following facilites. \
\
    o Read Open Type Layout Tables from OTF file.  Currently these \
      tables are supported; head, name, cmap, GDEF, GSUB, and GPOS. \
\
    o Convert a Unicode character sequence to a glyph code sequence by \
      using the above tables. \
 \
The combination of libotf and the FreeType library (Ver.2) realizes \
CTL (complex text layout) by OpenType fonts."

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=7fbc338309ac38fefcd64b04bb903e34"

SRC_URI = "http://download.savannah.gnu.org/releases/m17n/libotf-${PV}.tar.gz \
           file://0001-Do-not-build-example-folder.patch \
           file://0002-Use-pkg-config-for-freetype.patch \
           "
SRC_URI[md5sum] = "9b0b708ba5de53bf83e1cb09c6a6e100"
SRC_URI[sha256sum] = "68db0ca3cda2d46a663a92ec26e6eb5adc392ea5191bcda74268f0aefa78066b"

DEPENDS = "freetype"

BINCONFIG = "${bindir}/libotf-config"
inherit autotools pkgconfig binconfig-disabled

PACKAGECONFIG = "${@bb.utils.filter('DISTRO_FEATURES', 'x11', d)}"
PACKAGECONFIG[x11] = "--with-x=yes,--with-x=no,libice libx11 libxaw"
