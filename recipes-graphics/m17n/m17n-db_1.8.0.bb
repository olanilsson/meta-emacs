SUMMARY = "multilingual text processing library - database"
DESCRIPTION = "This package contains the database files used by m17n-lib."
HOMEPAGE = "https://http://www.nongnu.org/m17n/"

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"

SRC_URI = "http://download.savannah.nongnu.org/releases/m17n/m17n-db-${PV}.tar.gz"
SRC_URI[md5sum] = "49378f8ed738f84abfaf5e09699e1fa0"
SRC_URI[sha256sum] = "657f23835b6655e7a63a362bac66260454ee356da4855eadb9366911d33fdc6d"

inherit gettext autotools-brokensep

FILES_${PN} += "${datadir}/m17n/"
# gawk is required by /usr/share/m17n/scripts/tbl2mim.awk
RDEPENDS_${PN} += "gawk"
