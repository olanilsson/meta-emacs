LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"

SRC_URI = "http://download.savannah.gnu.org/releases/m17n/m17n-lib-${PV}.tar.gz \
           file://0001-Use-pkg-config-for-libxml2.patch \
           file://0002-Check-and-init-pkg-config-m4-macros.patch \
           file://0003-Use-pkg-config-for-libfribidi.patch \
           file://0004-Use-pkg-config-for-libotf.patch \
           file://0005-Use-pkg-config-for-freetype.patch \
           file://0006-Use-pkg-config-for-xft2.patch \
           file://0007-Use-PKG_CHECK_MODULES-fontconfig.patch \
           "
SRC_URI[md5sum] = "35a7c29b4c5892c643153eeaefd1f787"
SRC_URI[sha256sum] = "78bacae7451822dfff62482ce4f9433c5ae82846e4c64b590541e29e800fe64a"

S = "${WORKDIR}/m17n-lib-${PV}"

X11CONFIGS = "x11 xaw xft"
PACKAGECONFIG = "m17n-db gui fribidi libotf freetype fontconfig gd libxml2 anthy thai"
PACKAGECONFIG += "${@bb.utils.contains('DISTRO_FEATURES', 'x11', '${X11CONFIGS}', '', d)}"
PACKAGECONFIG[xaw] = ",,libxaw"
PACKAGECONFIG[m17n-db] = ",,m17n-db"
PACKAGECONFIG[gui] = "--enable-gui,--disable-gui"
PACKAGECONFIG[x11] = ",,virtual/libx11"
PACKAGECONFIG[fribidi] = ",,fribidi"
PACKAGECONFIG[libotf] = "--with-libotf,--without-libotf,libotf"
PACKAGECONFIG[freetype] = ",,freetype"
PACKAGECONFIG[xft] = ",,libxft"
PACKAGECONFIG[fontconfig] = "--with-fontconfig,--without-fontconfig,fontconfig"
PACKAGECONFIG[gd] = "--with-gd,--without-gd,gd"
PACKAGECONFIG[libxml2] = ",,libxml2"
PACKAGECONFIG[anthy] = ",,anthy"
PACKAGECONFIG[thai] = ",,libthai"
PACKAGECONFIG[wordcut] = ",,wordcut"

# m17n-flt installs a script m17n-config, which is deprecated in favor of
# pkg-config.  Use binconfig-disabled to break the m17n-config.
BINCONFIG = "m17n-config"
inherit pkgconfig gettext autotools binconfig-disabled

# Parallel build does not work
EXTRA_OEMAKE_append = " -j1"

# Split libs into several packages to enable separate installs
PACKAGE_BEFORE_PN = "${PN}-bin ${PN}-mimx ${PN}-gui ${PN}-core ${PN}-shell"
FILES_${PN}-bin = "${bindir}"
FILES_${PN}-core = "${libdir}/*-core${SOLIBS}"
FILES_${PN}-shell = "${libdir}/libm17n${SOLIBS}"
FILES_${PN}-gui = "${libdir}/*-gui${SOLIBS} ${libdir}/m17n/1.0/libm17n-*.so"
FILES_${PN}-mimx = "${libdir}/m17n/1.0/libmimx-*.so"
