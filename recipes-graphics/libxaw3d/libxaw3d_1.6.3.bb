LICENSE = "X11"
LIC_FILES_CHKSUM = "file://COPYING;md5=8d5e9a1707c9f4300b992b34e21ac54c"

SRC_URI = "https://xorg.freedesktop.org/releases/individual/lib/libXaw3d-${PV}.tar.bz2"
SRC_URI[md5sum] = "35b9296b8b2fccd4f46480c0afbd7f4f"
SRC_URI[sha256sum] = "2dba993f04429ec3d7e99341e91bf46be265cc482df25963058c15f1901ec544"

S = "${WORKDIR}/libXaw3d-${PV}"

# NOTE: unable to map the following pkg-config dependencies: xpm)
#       (this is based on recipes that have previously been built and packaged)
DEPENDS = "flex-native libxext libx11 libxt libxmu bison-native"

# NOTE: if this software is not capable of being built in a separate build directory
# from the source, you should replace autotools with autotools-brokensep in the
# inherit line
inherit pkgconfig autotools distro_features_check
		
REQUIRED_DISTRO_FEATURES = "x11"		

# Specify any options you want to pass to the configure script using EXTRA_OECONF:
EXTRA_OECONF = "--enable-gray-stipples --enable-arrow-scrollbars"

PACKAGECONFIG = "xpm"
PACKAGECONFIG[xpm] = "--enable-multiplane-bitmaps,--disable-multiplane-bitmaps,libxpm"
			  			 
