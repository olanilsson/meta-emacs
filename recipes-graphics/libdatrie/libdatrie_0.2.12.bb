SUMMARY = "datrie - Double-Array Trie Library"
DESCRIPTION = "This is an implementation of double-array structure for representing trie, \
as proposed by Junichi Aoe."
HOMEPAGE = "https://linux.thai.net/~thep/datrie/"

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=2d5025d4aa3495befef8f17206a5b0a1"

DEPENDS = "autoconf-archive-native"

SRC_URI = "https://linux.thai.net/pub/thailinux/software/libthai/libdatrie-${PV}.tar.xz"
SRC_URI[md5sum] = "b2243d583e25925200c134fad9f2fb50"
SRC_URI[sha256sum] = "452dcc4d3a96c01f80f7c291b42be11863cd1554ff78b93e110becce6e00b149"

inherit autotools

EXTRA_OECONF = "--disable-doxygen-doc"

PACKAGE_BEFORE_PN = "${PN}-bin"
FILES_${PN}-bin = "${bindir}"

BBCLASSEXTEND = "native"
