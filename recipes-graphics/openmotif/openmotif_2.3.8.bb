SUMMARY = "The Motif user interface component toolkit"
HOMEPAGE = "https://motif.ics.com/"

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

SRC_URI = "https://sourceforge.net/projects/motif/files/Motif%20${PV}%20Source%20Code/motif-${PV}.tar.gz \
           file://0001-Replace-INCLUDES-with-AM_CPPFLAGS-in-all-Makefile.am.patch \
           file://0002-Add-foreign-Automake-option-to-master-Makefile.am.patch \
           file://0003-Make-sure-Wformat-security-is-not-an-error-for-libmr.patch \
           file://0004-Run-makestrs-in-user-qemu.patch \
           file://0005-Make-sure-Wformat-security-is-not-an-error-for-libwm.patch \
           file://0006-Do-not-build-demos.patch \
           file://0007-Make-lib-Xm-Makefile-work-in-parallel-builds.patch \
           file://0008-Run-wmluiltok-and-wml-in-user-qemu.patch \
           file://0009-Run-wmldbcreate-in-user-qemu.patch \
           file://0010-Detect-freetype-using-pkg-config.patch \
           file://0011-Fix-the-use-of-x_include-in-xrender-detection.patch \
           file://0012-Detect-fontconfig-using-pkg-config.patch \
           "
SRC_URI[md5sum] = "7572140bb52ba21ec2f0c85b2605e2b1"
SRC_URI[sha256sum] = "859b723666eeac7df018209d66045c9853b50b4218cecadb794e2359619ebce7"

S = "${WORKDIR}/motif-${PV}"

# There is no recipe for libXp - X print support.  Otherwise it should
# be added to the DEPENDS.  Add --disable-printing to EXTRA_OECONF instead.
DEPENDS = "byacc-native flex flex-native freetype libx11 libxmu xbitmaps"

inherit autotools qemucmd distro_features_check pkgconfig

CONFIGURE_FILES += "ac_find_xft.m4"

REQUIRED_DISTRO_FEATURES = "x11"

# Make configure skip searching for the X11 header path.  Assume
# headers are installed in /usr/include/X11
EXTRA_OECONF = " \
  ac_cv_file__usr_X_include_X11_X_h=no \
  ac_cv_file__usr_X11R6_include_X11_X_h=no \
  --disable-printing \
  --disable-motif22-compatibility \
  --with-x \
"

PACKAGECONFIG = "xft jpeg png"
PACKAGECONFIG[messagecatalog] = "--enable-message-catalog,--disable-message-catalog"
PACKAGECONFIG[themes] = "--enable-themes,--disable-themes"
PACKAGECONFIG[debugthemes] = "--enable-themes --enable-debug-themes,--disable-debug-themes"
PACKAGECONFIG[xft] = "--enable-xft,--disable-xft,libxft libxrender"
PACKAGECONFIG[jpeg] = "--enable-jpeg,--disable-jpeg,jpeg"
PACKAGECONFIG[png] = "--enable-png,--disable-png,libpng"

QEMUCMD_WMLDBCCREATE = "${@qemucmd(d, ['${B}/lib/Xm/.libs'])}"
QEMUCMD_WMLDBCCREATE_class-native = ""
export QEMUCMD_WMLDBCCREATE

PACKAGE_BEFORE_PN = "${PN}-uil ${PN}-mwm ${PN}-libmrm ${PN}-libuil ${PN}-libxm"

RDEPENDS_${PN}-libxm += "${PN}"
RPROVIDES_${PN} += "motif-common"
RPROVIDES_${PN}-uil += "uil"
RPROVIDES_${PN}-mwm += "mwm"
RPROVIDES_${PN}-libmrm += "libmrm"
RPROVIDES_${PN}-libuil += "libuil"
RPROVIDES_${PN}-libxm += "libxm"

FILES_${PN} += "${libdir}/X11"
FILES_${PN}-uil = "${bindir}/uil"
FILES_${PN}-mwm = "${bindir}/mwm ${bindir}/xmbind "
FILES_${PN}-libmrm = "${libdir}/libMrm${SOLIBS}"
FILES_${PN}-libuil = "${libdir}/libUil${SOLIBS}"
FILES_${PN}-libxm = "${libdir}/libXm${SOLIBS}"

BBCLASSEXTEND = "native"
